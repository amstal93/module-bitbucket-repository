terraform {
  required_version = ">= 0.14.9"

  required_providers {
    bitbucket = {
      source  = "aeirola/bitbucket"
      version = "~> 2.0"
    }
  }
}
